\section{Main Results}

\begin{frame}{Problems \& Solutions}

    \pause

    Several difficulties:

    \begin{itemize}
        \item\pause Disorder
        \item\pause Special disorder - strong and singular
        \item\pause Non-local kernel
    \end{itemize}

    \pause

    Grand canonical ensemble - Major analytical simplification.

    \pause

    \begin{equation}
        \label{Eq:GrandCanonicalGE}
        \mathrm{Err}\left(g,\eta\right) = e^{-\eta} \sum_{n=0}^\infty \frac{\eta^n}{n!}
        \underbrace{\intop_{x\in\mathbb{X}}d\mu_x \left\langle\left(g^*\left(x\right)-g\left(x\right)\right)^2\right\rangle_{\mathcal{X}\sim \mu^n}}_{\mathrm{Err}\left(g,n\right)}
    \end{equation}

    \pause

    The main focus of this work is on calculating the grand canonical generalization error, and its dependence on $\eta$ - the \emph{grand canonical learning curve}.

    \pause

    For a DNN with a fully connected first layer, the kernel obeys a rotational symmetry and therefore can be diagonalized using the hyperspherical harmonics.

\end{frame}

\begin{frame}{Sub-leading Corrections to the GP Learning Curve I}

    \begin{itemize}
        \item\pause
              Calculating the grand canonical prediction, interestingly it holds that
              \begin{equation}
                  \left\langle g^*\left(x_*\right)\right\rangle_\eta =  g^*_{EK,\eta}\left(x_*\right) +O(1/\eta^2)
              \end{equation}
              where $g^*_{EK,\eta}\left(x_*\right)$ is the EK prediction as defined in Eq. \eqref{Eq:EK}, with $\eta$ replacing $N$.
        \item\pause
              Sub-leading (SL) corrections to the EK result:
              \begin{align}
                  \label{eq:g_nextOrderCorrection}
                  \left\langle g^*\left(x_*\right) \right\rangle_\eta & = g^*_{EK,\eta}\left(x_*\right) + g^*_{SL,\eta}\left(x_*\right) + O(1/\eta^3)                                                                                                                       \\
                  \label{eq:g_SL_explicit}
                  g^*_{SL,\eta}\left(x_*\right)                       & = -\frac{\eta}{\sigma^{4}}\sum_{i,j,k} \Lambda_{i,j,k}
                  g_{i}\phi_{j}\left(x_{*}\right) \intop d\mu_{x}\phi_{i}\left(x\right)\phi_{j}\left(x\right)\phi_{k}^{2}\left(x\right)                                                                                                                                     \\
                  \Lambda_{i,j,k}                                     & = \frac{\frac{\sigma^{2}}{\eta}}{\lambda_{i}+\frac{\sigma^{2}}{\eta}}\left(\frac{1}{\lambda_{j}}+\frac{\eta}{\sigma^{2}}\right)^{-1}\left(\frac{1}{\lambda_{k}}+\frac{\eta}{\sigma^{2}}\right)^{-1}
              \end{align}
              where $\phi_i$ and $\lambda_i$ are the eigenfunctions and eigenvalues of the kernel, correspondingly.
    \end{itemize}

\end{frame}

\begin{frame}{Sub-leading Corrections to the GP Learning Curve II}

    \pause

    Similar expressions to $\left\langle {g^{*}}^{2}\left(x_*\right) \right\rangle_\eta$ are calculated to yield

    \begin{align}
        \left\langle {g^{*}}^{2}\left(x_*\right) \right\rangle_\eta & = {g^{*}_{EK,\eta}}^{2}\left(x_*\right)  + O(1/\eta^2)                                                                          \\
                                                                    & = {g^{*}_{EK,\eta}}^{2}\left(x_*\right) + 2\cdot g^*_{EK,\eta}\left(x_*\right)\cdot g^*_{SL,\eta}\left(x_*\right) + O(1/\eta^3)
    \end{align}

    allowing us to calculate the grand canonical generalization error

    \begin{align}
        \mathrm{Err}(g, \eta) & = \intop d\mu_x \left\langle \left[g^*\left(x\right)-g\left(x\right)\right]^2 \right\rangle_\eta                            \\
                              & = \intop d\mu_x \left[g^*_{EK,\eta}\left(x_*\right) -g\left(x\right)\right]^2  + O(1/\eta^2)                                \\
                              & = \intop d\mu_x  \left[g^*_{EK,\eta}\left(x_*\right) + g^*_{SL,\eta}\left(x_*\right)-g\left(x\right)\right]^2 + O(1/\eta^3)
    \end{align}

\end{frame}

\begin{frame}{Sub-leading Corrections to the GP Learning Curve III}

    \pause

    For the fully connected case, substituting the hyperspherical harmonics for the $\phi_i$s in Eq. \eqref{eq:g_SL_explicit}, the expression for $g^*_{SL,\eta}\left(x_*\right)$ simplifies significantly to yield

    \begin{equation}
        g^{*}_{SL,\eta}(x_{*}) = - \sum_{l,m}\frac{\eta^{-1} \lambda_l C_{K,\eta/\sigma^2}}{(\lambda_l+\sigma^2/\eta)^2}g_{lm} Y_{lm}\left(x_*\right)
    \end{equation}

    where $C_{K,\eta/\sigma^2} = \sum_{l} \deg\left(l\right) (\lambda_{l}^{-1} + \eta/\sigma^2)^{-1}$, and notably cross-talk between eigenfunctions has been eliminated.

    \pause

    We can see that the correction is always negative, meaning that the EK approximation is too optimist.

\end{frame}

\begin{frame}{Numerical Verification I}

    \begin{figure}[H]
        \centering
        \includegraphics[width=\linewidth]{assets/nngp_lc_prediction_plot_prx.png}
        \caption{This graph is attributed to Or Malka.}
        \label{Fig:poisson_averaged_mse}
    \end{figure}

\end{frame}

\begin{frame}{Numerical Verification II}

    \begin{itemize}
        \item\pause One immediately notices that both predictions converge to the experimental learning curve when $\sigma^2/\eta\ll1$, and that the SL prediction is a notable improvement over the standard EK result. Another interesting feature is evident in the small $\eta$ regime of our results.
        \item\pause It has been shown that under certain conditions generalization error can actually increase with size of the training set in the small $N$ regime, as is demonstrated by our experimental data. This effect is completely missed by the EK prediction, which always predicts monotonically decreasing learning curves as evident in \eqref{Eq:EK}. The corrected SL predictions, on the other hand, show a potential of capturing this phenomenon.
    \end{itemize}

\end{frame}

\begin{frame}{Main Results}
    \begin{itemize}
        \item\pause This was the "climax" of the talk.
        \item\pause In the next parts, I will show the derivation of these results, why is this derivation robust, and discuss future directions.
    \end{itemize}
\end{frame}
